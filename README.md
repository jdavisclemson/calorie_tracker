Note that by using this software, you accept all liability for anything that occurs as a result of its usage.
Furthermore, by using this software, you acknowledge the fact that you understand that the author is not a medical professional.
There is also no warranty at all on the software, and its proper functioning is not guaranteed.
This software only provides a way to track historical data, and it provides estimates of future data.
Any data that is not directly entered by the user is only an estimate, and the accuracy is not guaranteed.

Lastly, please note that the first revision of this software was developed hastily, and so the code is a bit of a mess.
  This will be cleaned up in subsequent versions.

To Install:

  First and foremost, if you are using mac os, you should follow the instructions for Linux.
    Please know that I have almost no experience with mac os.
    I believe the Linux instructions will work for you, since mac os is a Unix OS.
    I'm not certain of this though.
  Second, while I have plenty of experience with windows, I do not own any instance of it, so it has not been tested on windows.
    I'm pretty certain that the windows instructions will work, but again, I can't give any guarantees.

  1) Clone this repository to whatever location you want the calorie tracker to reside.
  2) Ensure you have python installed.
       If you're using windows, make sure to check the option to add python to the path, and allow it to disable the path length limit if it gives you that option.
  3) In a terminal (or the command prompt on windows), navigate to the calorie tracker directory.
  4) Run "pip install -r requirements.txt".
  5) Now run "python calorie_tracker.py". This should launch the tracker.
  6) If you're on Linux, running "chmod +x calorie_tracker.py" may make life easier, as it will give executable permissions to the script.
       This will allow you to run it by clicking on it, or by passing the path to calorie_tracker.py to the shell.
       You can also right click on most GUI menu launchers and add an entry for the calorie tracker.
       You can find the icon at <calorie-tracker-directory>/resources/pictures/calorie_tracker_logo.png.
  7) If you're on windows, I would recommend creating a batch script that launches the calorie tracker. Then, you can create a shortcut to that.
       This shortcut can then be placed on the desktop.
       I think you can also right click and pin to start, or you could just paste the shortcut in the appropriate directory.
       You can change the icon for the shortcut to the one found at <calorie-tracker-directory>\resources\pictures\calorie_tracker_logo.png
