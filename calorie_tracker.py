#!/usr/bin/env python

import sys, time, os, json

from PyQt5.QtWidgets import (
		QMainWindow, QWidget, QMenuBar, QMenu, QComboBox, QTabWidget, QApplication)

from resources.extended_widgets import *
from resources.tab_views import *


class MainWindow(QMainWindow):

	def __init__(self, *args, **kwargs):
		super().__init__()
		self.viewDate = time.strftime("%Y%m%d")

		self.rootdir = os.path.dirname(os.path.realpath(__file__))
		self.datadir = os.path.join(self.rootdir, 'data')
		self.resourcedir = os.path.join(self.rootdir, 'resources')
		self.picturedir = os.path.join(self.resourcedir, 'pictures')

		if not os.path.exists(self.datadir):
			os.makedirs(self.datadir)

		self.user_path = os.path.join(self.datadir, "user.txt")
		self.user_data_path = os.path.join(self.datadir, "users.json")

		self.drawMainWindow()
		self.read_data()
	
	def read_data(self):
		self.user_data = {}
		try:
			with open(self.user_data_path, 'r') as f:
				self.user_data = json.load(f)
		except: pass
		self.userWidget.clear()
		self.userWidget.addItem('')
		self.userWidget.addItems(self.user_data.keys())
		self.user = ''
		try:
			with open(self.user_path, 'r') as f:
				self.user = f.readline()
		except: pass
		self.userWidget.setCurrentText(self.user)
		self.dayView.update_display()
		self.trendView.update_display()
	
	def drawMainWindow(self):
		self.setWindowTitle("Calorie Tracker")
		self.setWindowIcon(QtGui.QIcon(os.path.join(self.picturedir, 'calorie_tracker_logo.png')))
		self.mainLayout = QVBoxLayout(self)
		self.createUserControls()
		self.createTabs()
		cw = QWidget()
		cw.setLayout(self.mainLayout)
		self.setCentralWidget(cw)
		self.show()

	def user_changed(self):
		with open(self.user_path, 'w') as f:
			f.write(self.userWidget.currentText())
		self.read_data()
		self.dayView.update_display()
		self.trendView.update_display()
	
	def createUserControls(self):
		userControlsLayout = QHBoxLayout()
		self.userWidget = QComboBox()
		self.userWidget.activated.connect(self.user_changed)
		editUsersButton = QPushButton("Edit Users")
		userControlsLayout.addWidget(self.userWidget)
		userControlsLayout.addWidget(editUsersButton)
		self.mainLayout.addLayout(userControlsLayout)
		editUsersButton.clicked.connect(self.showEditUsersDialog)
	
	def showEditUsersDialog(self):
		self.editUsersDialog = EditUsersDialog(self)
		self.editUsersDialog.show()
	
	def createTabs(self):
		tabsWidget = QTabWidget()
		self.dayView = DayView(self)
		self.trendView = TrendView(self)
		tabsWidget.addTab(self.dayView.mainWidget, "Day View")
		tabsWidget.addTab(self.trendView.mainWidget, "Trend View")
		self.mainLayout.addWidget(tabsWidget)


app = QApplication(sys.argv)
w = MainWindow()
app.exec_()

