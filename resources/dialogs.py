#!/usr/bin/env python

import time
from datetime import datetime
from dateutil.relativedelta import relativedelta
import json

from PyQt5.QtCore import QDate

from PyQt5.QtWidgets import (
		QDialog, QVBoxLayout, QHBoxLayout, QLabel, QLineEdit, QPushButton, QCalendarWidget, QRadioButton)

from resources.extended_widgets import *

class YesNoDialog(QDialog):

	def __init__(self, message=''):
		super().__init__()
		self.mainLayout = QVBoxLayout()
		self.labelMessage = QLabel(message)
		self.mainLayout.addWidget(self.labelMessage)
		self.buttonLayout = QHBoxLayout()
		self.buttonNo = QPushButton("No")
		self.buttonNo.clicked.connect(self.no_clicked)
		self.buttonLayout.addWidget(self.buttonNo)
		self.buttonYes = QPushButton("Yes")
		self.buttonYes.clicked.connect(self.yes_clicked)
		self.buttonLayout.addWidget(self.buttonYes)
		self.mainLayout.addLayout(self.buttonLayout)
		self.setLayout(self.mainLayout)
	
	def show(self, message = ''):
		self.user_result = False
		if message:
			self.labelMessage.setText(message)
		self.exec_()
	
	def no_clicked(self):
		self.user_result = False
		self.close()

	def yes_clicked(self):
		self.user_result = True
		self.close()

class UserInputDialog(QDialog):

	def __init__(self, message=''):
		super().__init__()
		self.mainLayout = QVBoxLayout()
		self.labelMessage = QLabel(message)
		self.mainLayout.addWidget(self.labelMessage)
		self.lineEditInput = QLineEdit()
		self.mainLayout.addWidget(self.lineEditInput)
		self.buttonLayout = QHBoxLayout()
		self.buttonCancel = QPushButton("Cancel")
		self.buttonCancel.clicked.connect(self.close)
		self.buttonLayout.addWidget(self.buttonCancel)
		self.buttonOkay = QPushButton("Okay")
		self.buttonOkay.clicked.connect(self.okay_clicked)
		self.buttonLayout.addWidget(self.buttonOkay)
		self.mainLayout.addLayout(self.buttonLayout)
		self.setLayout(self.mainLayout)
	
	def show(self, message=''):
		self.canceled = True
		self.input = ''
		if message:
			self.labelMessage.setText(message)
		self.exec_()

	def okay_clicked(self):
		self.input = self.lineEditInput.text()
		self.canceled = False
		self.close()

class MessageDialog(QDialog):

	def __init__(self, message=''):
		super().__init__()
		self.mainLayout = QVBoxLayout()
		self.labelMessage = QLabel(message)
		self.buttonClose = QPushButton("Close")
		self.buttonClose.clicked.connect(self.close)
		self.mainLayout.addWidget(self.labelMessage)
		self.mainLayout.addWidget(self.buttonClose)
		self.setLayout(self.mainLayout)
	
	def show(self, message):
		if message:
			self.labelMessage.setText(message)
		self.exec_()


class DateDialog(QDialog):

	def __init__(self, initial_date=''):
		self.date = initial_date
		self.date_selected = False
		super().__init__()
#		if not len(self.date):
#			self.date = time.strftime('%Y%m%d')
		self.setWindowTitle("Select Day")
		self.mainLayout = QVBoxLayout()
		self.calendarWidget = QCalendarWidget()
#		self.calendarWidget.setSelectedDate(QDate.fromString(self.date, 'yyyyMMdd'))
		self.mainLayout.addWidget(self.calendarWidget)
		self.setLayout(self.mainLayout)
		self.calendarWidget.clicked.connect(self.calendarWidgetClicked)

	def calendarWidgetClicked(self):
		self.date_selected = True
		qtdate = self.calendarWidget.selectedDate()
		year = str(qtdate.year()).rjust(4, '0')
		month = str(qtdate.month()).rjust(2, '0')
		day = str(qtdate.day()).rjust(2, '0')
		self.date = year + month + day
		self.close()

	def show(self, initial_date=''):
		if initial_date:
			self.date = initial_date
		if not self.date:
			self.date = time.strftime('%Y%m%d')
		self.calendarWidget.setSelectedDate(QDate.fromString(self.date, 'yyyyMMdd'))
		self.exec_()


class EditUsersDialog(QDialog):

	def __init__(self, parent):
		self.parent = parent
		super().__init__()
		self.setWindowTitle("Edit Users")
		self.mainLayout = QVBoxLayout()
		self.mainLayout.addWidget(QLabel("Name:"))
		self.lineEditUserName = QLineEdit()
		self.lineEditUserName.textChanged.connect(self.find_user)
		self.mainLayout.addWidget(self.lineEditUserName)

		self.birthday = ''
		self.buttonSelectBirthday = QPushButton("Select Birthday")
		self.buttonSelectBirthday.clicked.connect(self.select_birthday)
		self.labelBirthday = DateLabel('Birthday: ')
		self.labelAge = QLabel("Age: ")
		self.mainLayout.addWidget(self.buttonSelectBirthday)
		self.mainLayout.addWidget(self.labelBirthday)
		self.mainLayout.addWidget(self.labelAge)

		self.person_height = ''
		layoutHeight = QHBoxLayout()
		self.labelHeight = QLabel("Height (in):")
		layoutHeight.addWidget(self.labelHeight)
		self.lineEditHeight = QLineEdit()
		self.lineEditHeight.textChanged.connect(self.set_height)
		layoutHeight.addWidget(self.lineEditHeight)
		self.mainLayout.addLayout(layoutHeight)

		self.gender = ''
		layoutGender = QVBoxLayout()
		self.radioButtonMale = QRadioButton("Male")
		self.radioButtonFemale = QRadioButton("Female")
		self.radioButtonMale.toggled.connect(self.set_gender)
		self.radioButtonFemale.toggled.connect(self.set_gender)
		layoutGender.addWidget(self.radioButtonMale)
		layoutGender.addWidget(self.radioButtonFemale)
		self.mainLayout.addLayout(layoutGender)

		buttonLayout = QHBoxLayout()
		cancelButton = QPushButton("Cancel")
		saveButton = QPushButton("Save")
		deleteButton = QPushButton("Delete")
		buttonLayout.addWidget(cancelButton)
		buttonLayout.addWidget(saveButton)
		buttonLayout.addWidget(deleteButton)
		self.mainLayout.addLayout(buttonLayout)
		self.setLayout(self.mainLayout)
		saveButton.clicked.connect(self.save_user)
		cancelButton.clicked.connect(self.close)
		deleteButton.clicked.connect(self.delete_user)
		self.lineEditUserName.setText(self.parent.user)
	
	def delete_user(self):
		current_user = self.lineEditUserName.text()
		ynd = YesNoDialog(f"Are you sure you want to delete the user: {current_user} ?")
		ynd.show()
		if not ynd.user_result:
			return
		try:
			del self.parent.user_data[current_user]
			if self.parent.user == current_user:
				with open(self.parent.user_path, 'w') as f:
					f.write('')
			with open(self.parent.user_data_path, 'w') as f:
				json.dump(self.parent.user_data, f, indent=4)
		except: pass
		self.parent.read_data()
		self.close()
	
	def set_height(self):
		self.person_height = self.lineEditHeight.text()
	
	def set_gender(self, checked):
		#When one radio button toggles, it causes another to toggle as well.
		#  This if statement prevents running this method twice.
		if not checked:
			return
		if self.radioButtonMale.isChecked():
			self.gender = 'm'
		elif self.radioButtonFemale.isChecked():
			self.gender = 'f'
		else:
			self.gender = ''

	def select_birthday(self):
		dd = DateDialog(self.birthday)
		dd.show()
		if dd.date_selected:
			self.birthday = dd.date
			self.update_display_info()
	
	def update_display_info(self):
		age_label_text = "Age: "
		self.labelBirthday.setDate('')
		if len(self.birthday):
			birthday = datetime.strptime(self.birthday, '%Y%m%d').date()
			now = datetime.today().date()
			self.age = relativedelta(now, birthday).years
			age_label_text += f"{self.age:.0f}"
			self.labelBirthday.setDate(self.birthday)
		self.lineEditHeight.setText(self.person_height)
		self.radioButtonMale.setAutoExclusive(False)
		self.radioButtonFemale.setAutoExclusive(False)
		self.radioButtonMale.setChecked(False)
		self.radioButtonFemale.setChecked(False)
		self.radioButtonMale.setAutoExclusive(True)
		self.radioButtonFemale.setAutoExclusive(True)
		if self.gender == 'm':
			self.radioButtonMale.setChecked(True)
		elif self.gender == 'f':
			self.radioButtonFemale.setChecked(True)
		self.labelAge.setText(age_label_text)
	
	def find_user(self):
		self.birthday = ''
		self.gender = ''
		self.person_height = ''
		self.dates = {}
		current_user = self.lineEditUserName.text()
		try:
			self.birthday = self.parent.user_data[current_user]['birthday']
		except: pass
		try:
			self.gender = self.parent.user_data[current_user]['gender']
		except: pass
		try:
			self.person_height = self.parent.user_data[current_user]['height']
		except: pass
		try:
			self.dates = self.parent.user_data[current_user]['dates']
		except: pass
		self.update_display_info()
	
	def save_user(self):
		current_user = self.lineEditUserName.text()
		if not len(current_user.strip()):
			md = MessageDialog()
			md.show('Cannot save blank user.')
			return
		data_incomplete = False
		if not self.birthday:
			data_incomplete = True
		if not self.gender:
			data_incomplete = True
		if not self.person_height:
			data_incomplete = True
		if data_incomplete:
			md = MessageDialog()
			md.show('Please populate all of the information.')
			return
		with open(self.parent.user_path, 'w') as f:
			f.write(current_user)
		self.parent.user_data[current_user] = {'birthday' : self.birthday, 'gender' : self.gender, 'height' : self.person_height, 'dates' : self.dates}
		with open(self.parent.user_data_path, 'w') as f:
			json.dump(self.parent.user_data, f, indent=4)
		self.parent.read_data()
		self.close()
	
	def show(self):
		self.exec_()

class DateOffsetDialog(QDialog):

	def __init__(self):
		super().__init__()
		self.years = ''
		self.months = ''
		self.days = ''
		self.canceled = True
		self.draw()
	
	def draw(self):
		mainLayout = QVBoxLayout()

		layoutYear = QHBoxLayout()
		labelYear = QLabel("Years: ")
		layoutYear.addWidget(labelYear)
		self.lineEditYears = QLineEdit()
		layoutYear.addWidget(self.lineEditYears)
		mainLayout.addLayout(layoutYear)

		layoutMonth = QHBoxLayout()
		labelMonth = QLabel("Months: ")
		layoutMonth.addWidget(labelMonth)
		self.lineEditMonths = QLineEdit()
		layoutMonth.addWidget(self.lineEditMonths)
		mainLayout.addLayout(layoutMonth)

		layoutDay = QHBoxLayout()
		labelDay = QLabel("Days: ")
		layoutDay.addWidget(labelDay)
		self.lineEditDays = QLineEdit()
		layoutDay.addWidget(self.lineEditDays)
		mainLayout.addLayout(layoutDay)

		layoutButtons = QHBoxLayout()
		self.buttonCancel = QPushButton("Cancel")
		self.buttonCancel.clicked.connect(self.cancelClicked)
		layoutButtons.addWidget(self.buttonCancel)
		self.buttonOkay = QPushButton("Okay")
		self.buttonOkay.clicked.connect(self.okayClicked)
		layoutButtons.addWidget(self.buttonOkay)
		mainLayout.addLayout(layoutButtons)
		self.setLayout(mainLayout)

	def cancelClicked(self):
		self.canceled = True
		self.close()

	def okayClicked(self):
		self.canceled = False
		self.years = self.lineEditYears.text()
		self.months = self.lineEditMonths.text()
		self.days = self.lineEditDays.text()
		self.close()

	def show(self):
		self.exec_()




