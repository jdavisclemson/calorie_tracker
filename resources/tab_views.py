#!/usr/bin/env python

import os

from PyQt5.QtWidgets import (
		QWidget, QVBoxLayout, QHBoxLayout, QPushButton, QLineEdit)

from PyQt5 import QtCore
from PyQt5 import QtGui

from datetime import datetime, timedelta

from resources.extended_widgets import *
from resources.dialogs import *

from PyQt5.QtWidgets import QLabel, QApplication, QLineEdit, QGridLayout, QRadioButton, QTableWidget, QCheckBox, QTableWidgetItem, QHeaderView

from copy import deepcopy


class Utils():

	def __init__(self):
		pass

	@staticmethod
	def date_diff(d2, d1):
		d2 = datetime.strptime(d2, '%Y%m%d')
		d1 = datetime.strptime(d1, '%Y%m%d')
		return (d2 - d1).days

	@staticmethod
	def increment_date(date, years=0, months=0, days=0):
		return (datetime.strptime(date, '%Y%m%d') + relativedelta(years=years, months=months, days=days)).strftime('%Y%m%d')


class DataComputation():

	def __init__(self, parent):
		self.parent = parent
	
	def get_dates_with_measured_weights(self):
		try:
			user = self.parent.user_data[self.parent.user]
			dates_with_measured_weights = [val for val in user['dates'].keys() if user['dates'][val]['measured_weight']]
			dates_with_measured_weights.sort()
		except:
			return []
		return dates_with_measured_weights

	def get_dates_with_complete_data(self):
		try:
			user = self.parent.user_data[self.parent.user]
			dates_with_complete_data = [val for val in user['dates'].keys() if user['dates'][val]['data_complete']]
			return dates_with_complete_data
		except:
			return []

	def average_calories_between_dates(self, d1, d2):
		if Utils.date_diff(d2, d1) < 0:
			return None
		dates_with_complete_data = self.get_dates_with_complete_data()
		dates_to_average = []
		for date in dates_with_complete_data:
			if Utils.date_diff(date, d1) < 0:
				continue
			if Utils.date_diff(d2, date) < 0:
				continue
			dates_to_average.append(date)
		if not dates_to_average:
			md = MessageDialog()
			md.show("Could not find any dates with complete data in the chosen timeframe.")
			return None
		total = 0
		count = 0
		for date in dates_to_average:
			count += 1
			calories_consumed = self._calories_consumed(date)
			if calories_consumed is not None:
				total += calories_consumed
			else:
				return None
		average = total / count
		return average

	def compute_calories_burned(self, gender, weight, height, age, activity_level):
		#Compute RMR, using the Mifflin-St Jeor equation...
		weight = weight / 2.2  #Convert pounds to kilograms
		height = height * 2.54 #Convert inches to centimeters
		if gender == 'm':
			rmr = (10 * weight) + (6.25 * height) - (5 * age) + 5
		elif gender == 'f':
			rmr = (10 * weight) + (6.25 * height) - (5 * age) - 161
		else:
			rmr = 0
		#Get total calories from RMR and exersize multiplication factor...
		if activity_level == 'none':
			total_calories = rmr * 1.2
		elif activity_level == 'medium':
			total_calories = rmr * 1.55
		elif activity_level == 'intense':
			total_calories = rmr * 1.9
		else:
			total_calories = 0
		return total_calories

	def _calories_consumed(self, date):
		try:
			diet = self.parent.user_data[self.parent.user]['dates'][date]['diet']
		except:
			return None
		calories_consumed = 0
		for record in diet:
			try:
				calories_consumed += float(record[1])
			except:
				pass
		return calories_consumed

	def _most_recent_date_index(self, date, dates):
		date_index = -1
		for tempdate in dates:
			datediff = (datetime.strptime(date, '%Y%m%d') - datetime.strptime(tempdate, '%Y%m%d')).days
			if datediff >= 0:
				date_index += 1
			else:
				break
		return date_index

	def compute_forecast(self, end_date, daily_calories, activity_level):
		user = self.parent.user_data[self.parent.user]
		today = datetime.today().strftime('%Y%m%d')
		predicted_weight = float(self.compute_data(today)['predicted_weight'])
		tempdate = Utils.increment_date(today, days=1)
		X = []
		Y = []
		while Utils.date_diff(end_date, tempdate) >= 0:
			age = relativedelta(datetime.strptime(tempdate, '%Y%m%d'), datetime.strptime(user['birthday'], '%Y%m%d')).years
			calories_consumed = float(daily_calories)
			calories_burned = self.compute_calories_burned(user['gender'], predicted_weight, float(user['height']), age, activity_level)
			calorie_difference = calories_consumed - calories_burned
			weight_difference = calorie_difference / 3500
			predicted_weight += weight_difference
			X.append(tempdate)
			Y.append(predicted_weight)
			tempdate = Utils.increment_date(tempdate, days=1)
		return (X, Y)
	
	def compute_data(self, date):
		result = {
				'last_measured_weight': '',
				'last_measured_date': '',
				'predicted_weight': '',
				'calories_consumed': '',
				'calories_burned': '',
				'calorie_difference': '',
				'weight_difference': ''
				}
		try:
			user = self.parent.user_data[self.parent.user]
		except:
			return result
		#Last measured weight and date...
		try:
			dates = list(user['dates'].keys())
		except: return result
		dates.sort()
		date_index = self._most_recent_date_index(date, dates)
		last_measured_weight = None
		last_measured_date = None
		for a in range(date_index, -1, -1):
			tempdate = dates[a]
			data = user['dates'][tempdate]
			if data['measured_weight']:
				last_measured_weight = float(data['measured_weight'])
				last_measured_date = tempdate
				result['last_measured_weight'] = data['measured_weight']
				try:
					formatted_date = tempdate[4:6] + '/' + tempdate[6:8] + '/' + tempdate[0:4]
				except:
					formatted_date = tempdate
				result['last_measured_date'] = formatted_date
				break
		#Predicted weight...
		if last_measured_date is None:
			return result
		lmd_index = self._most_recent_date_index(last_measured_date, dates)
		if last_measured_weight is None:
			return result
		predicted_weight = last_measured_weight
		for a in range(lmd_index, date_index + 1, 1):
			tempdate = dates[a]
			#Calories consumed...
			calories_consumed = self._calories_consumed(tempdate)
			#Calories burned...
			age = relativedelta(datetime.today().date(), datetime.strptime(user['birthday'], '%Y%m%d')).years
			calories_burned = self.compute_calories_burned(user['gender'], predicted_weight, float(user['height']), age, user['dates'][tempdate]['activity_level'])
			if tempdate == date:
				#Update the result dictionary.
				result['calories_consumed'] = f"{calories_consumed:.0f}"
				result['calories_burned'] = f"{calories_burned:.0f}"
			#If the data isn't complete, don't proceed.
			if not user['dates'][tempdate]['data_complete']:
				continue
			#Calorie difference...
			calorie_difference = calories_consumed - calories_burned
			#Weight difference...
			weight_difference = calorie_difference / 3500
			#Update predicted_weight...
			predicted_weight += weight_difference
			if tempdate == date:
				#Update the result dictionary.
				result['calorie_difference'] = f"{calorie_difference:.0f}"
				result['weight_difference'] = f"{weight_difference:.2f}"
		result['predicted_weight'] = f"{predicted_weight:.2f}"
		return result


class DayView():

	default_day_data = {
			'measured_weight' : '',
			'activity_level' : '',
			'diet' : [],
			'calories_consumed' : '',
			'calorie_difference' : '',
			'weight_difference' : '',
			'data_complete' : False}

	def __init__(self, parent):
		self.parent = parent
		self.data_computer = DataComputation(self.parent)
		try:
			self.day_data = self.parent.user_data[self.parent.user]['dates'][self.parent.viewDate]
		except:
			self.day_data = DayView.default_day_data
		self.drawMainWindow()

	def drawMainWindow(self):
		self.mainWidget = QWidget()
		self.mainLayout = QVBoxLayout(self.mainWidget)
		self.createInfoDisplay()
	
	def previous_date(self):
		inidate = datetime.strptime(self.parent.viewDate, '%Y%m%d')
		newdate = (inidate - timedelta(days=1)).strftime('%Y%m%d')
		self.parent.viewDate = newdate
		self.update_display()

	def next_date(self):
		inidate = datetime.strptime(self.parent.viewDate, '%Y%m%d')
		newdate = (inidate + timedelta(days=1)).strftime('%Y%m%d')
		self.parent.viewDate = newdate
		self.update_display()
	
	def view_today(self):
		self.parent.viewDate = datetime.today().strftime('%Y%m%d')
		self.update_display()

	def createInfoDisplay(self):
		spacing = 10
		layoutInfoDisplay = QVBoxLayout()
		layoutInfoDisplay.addSpacing(spacing)
		#Date controls - row 1
		layoutViewDate = QHBoxLayout()
		self.buttonPrev = QPushButton()
		self.buttonPrev.setIcon(QtGui.QIcon(os.path.join(self.parent.picturedir, "back_arrow.png")))
		self.buttonPrev.clicked.connect(self.previous_date)
		layoutViewDate.addWidget(self.buttonPrev)
		self.labelDate = DateLabel("Currently Viewing: ", self.parent.viewDate)
		layoutViewDate.addWidget(self.labelDate)
		self.buttonNext = QPushButton()
		self.buttonNext.setIcon(QtGui.QIcon(os.path.join(self.parent.picturedir, "forward_arrow.png")))
		self.buttonNext.clicked.connect(self.next_date)
		layoutViewDate.addWidget(self.buttonNext)
		layoutInfoDisplay.addLayout(layoutViewDate)
		#Date controls - row 2
		layoutDateButtons = QHBoxLayout()
		self.buttonSelectDate = QPushButton("Select Date")
		self.buttonSelectDate.clicked.connect(self.showDateDialog)
		layoutDateButtons.addWidget(self.buttonSelectDate)
		self.buttonToday = QPushButton("Today")
		self.buttonToday.clicked.connect(self.view_today)
		layoutDateButtons.addWidget(self.buttonToday)
		layoutInfoDisplay.addLayout(layoutDateButtons)
		#Horizontal line
		layoutInfoDisplay.addWidget(HorizontalLine())
		#Information display - left block
		layoutReadout_H = QHBoxLayout()
		layoutReadout_H.addSpacing(spacing)
		layoutReadout_V1 = QVBoxLayout()
		self.labelLastMeasuredWeight = PrefixedLabel("Last Measured Weight: ")
		self.labelLastMeasuredDate = PrefixedLabel("Last Measured Date: ")
		self.labelPredictedWeight = PrefixedLabel("Predicted Weight: ")
		self.labelWeightDifference = PrefixedLabel("Weight Difference: ")
		layoutReadout_V1.addWidget(self.labelLastMeasuredWeight)
		layoutReadout_V1.addWidget(self.labelLastMeasuredDate)
		layoutReadout_V1.addWidget(self.labelPredictedWeight)
		layoutReadout_V1.addWidget(self.labelWeightDifference)
		layoutReadout_H.addLayout(layoutReadout_V1)
		#Vertical line
		layoutReadout_H.addWidget(VerticalLine())
		#Information display - right block
		layoutReadout_H.addSpacing(spacing)
		layoutReadout_V2 = QVBoxLayout()
		self.labelCaloriesConsumed = PrefixedLabel("Calories Consumed: ")
		self.labelCaloriesBurned = PrefixedLabel("Calories Burned: ")
		self.labelCalorieDifference = PrefixedLabel("Calorie Difference: ")
		layoutReadout_V2.addWidget(self.labelCaloriesConsumed)
		layoutReadout_V2.addWidget(self.labelCaloriesBurned)
		layoutReadout_V2.addWidget(self.labelCalorieDifference)
		layoutReadout_H.addLayout(layoutReadout_V2)
		layoutInfoDisplay.addLayout(layoutReadout_H)
		#Horizontal line
		layoutInfoDisplay.addWidget(HorizontalLine())
		#Measured weight controls
		layoutMeasuredWeight = QHBoxLayout()
		labelMeasuredWeight = QLabel("Measured Weight (optional):")
		self.lineEditMeasuredWeight = QLineEdit()
		layoutMeasuredWeight.addWidget(labelMeasuredWeight)
		layoutMeasuredWeight.addWidget(self.lineEditMeasuredWeight)
		layoutInfoDisplay.addLayout(layoutMeasuredWeight)
		#Spacing	
		layoutInfoDisplay.addSpacing(spacing)
		#Activity level controls
		layoutActivityLevel = QHBoxLayout()
		labelExercise = QLabel("What was your activity level today?")
		layoutRadioButtons = QVBoxLayout()
		self.radioButtonNone = QRadioButton("Little to no exersize")
		self.radioButtonNone.setToolTip("Sedentary for most or all of the day.")
		self.radioButtonMedium = QRadioButton("Some exersize")
		self.radioButtonMedium.setToolTip("Active for part of the day (like going to the gym for 30 minutes to a few hours).")
		self.radioButtonIntense = QRadioButton("Intense exersize or sustained activity")
		self.radioButtonIntense.setToolTip("Active for a large portion of the day (like a physical job), or exersized intensely throughout the day (like athletic training).")
		layoutActivityLevel.addWidget(labelExercise)
		layoutRadioButtons.addWidget(self.radioButtonNone)
		layoutRadioButtons.addWidget(self.radioButtonMedium)
		layoutRadioButtons.addWidget(self.radioButtonIntense)
		layoutActivityLevel.addLayout(layoutRadioButtons)
		layoutInfoDisplay.addLayout(layoutActivityLevel)
		#Horizontal line
		layoutInfoDisplay.addWidget(HorizontalLine())
		#Diet controls
		layoutDiet = QVBoxLayout()
		self.tableDiet = QTableWidget()
		self.tableDiet.setRowCount(0)
		self.tableDiet.setColumnCount(2)
		self.tableDiet.setFixedHeight(210)
		self.tableDiet.setHorizontalHeaderLabels(["Food", "Calories"])
		self.tableDiet.cellChanged.connect(self.center_cell)
		header = self.tableDiet.horizontalHeader()
		header.setSectionResizeMode(0, QHeaderView.ResizeMode.Stretch)
		header.setSectionResizeMode(1, QHeaderView.ResizeMode.ResizeToContents)
		layoutDietButtons = QHBoxLayout()
		self.buttonAddRow = QPushButton("Add Row")
		self.buttonAddRow.clicked.connect(self.add_row)
		layoutDietButtons.addWidget(self.buttonAddRow)
		self.buttonDeleteRow = QPushButton("Delete Row")
		self.buttonDeleteRow.clicked.connect(self.delete_row)
		layoutDietButtons.addWidget(self.buttonDeleteRow)
		layoutDiet.addWidget(self.tableDiet)
		layoutDiet.addLayout(layoutDietButtons)
		layoutInfoDisplay.addLayout(layoutDiet)
		#Spacing
		layoutInfoDisplay.addSpacing(spacing)
		#Data complete checkbox
		self.checkBoxDataComplete = QCheckBox("Data is complete.")
		layoutInfoDisplay.addWidget(self.checkBoxDataComplete)
		#Horizontal line
		layoutInfoDisplay.addWidget(HorizontalLine())
		#Save and discard buttons
		layoutSaveDiscard = QHBoxLayout()
		self.buttonSaveData = QPushButton("Save")
		self.buttonSaveData.clicked.connect(self.save_data)
		layoutSaveDiscard.addWidget(self.buttonSaveData)
		self.buttonDiscard = QPushButton("Discard Unsaved Changes")
		self.buttonDiscard.clicked.connect(self.update_display)
		layoutSaveDiscard.addWidget(self.buttonDiscard)
		layoutInfoDisplay.addLayout(layoutSaveDiscard)
		self.mainLayout.addLayout(layoutInfoDisplay)
	
	def delete_row(self):
		uid = UserInputDialog("Row Number:")
		uid.show()
		if not uid.canceled:
			try:
				row = int(uid.input) - 1
				self.tableDiet.removeRow(row)
			except Exception as e:
				print(str(e))

	def add_row(self):
		self.tableDiet.setRowCount(self.tableDiet.rowCount() + 1)
	
	def center_cell(self, r, c):
		self.tableDiet.item(r, c).setTextAlignment(4)

	def update_display(self):
		self.labelDate.setDate(self.parent.viewDate)
		try:
			self.day_data = self.parent.user_data[self.parent.user]['dates'][self.parent.viewDate]
		except:
			self.day_data = deepcopy(DayView.default_day_data)
		computed_data = self.data_computer.compute_data(self.parent.viewDate)
		last_measured_weight = computed_data['last_measured_weight']
		last_measured_date = computed_data['last_measured_date']
		predicted_weight = computed_data['predicted_weight']
		calories_consumed = computed_data['calories_consumed']
		calories_burned = computed_data['calories_burned']
		calorie_difference = computed_data['calorie_difference']
		weight_difference = computed_data['weight_difference']
		self.labelLastMeasuredWeight.setPayload(last_measured_weight)
		self.labelLastMeasuredDate.setPayload(last_measured_date)
		self.labelPredictedWeight.setPayload(predicted_weight)
		self.lineEditMeasuredWeight.setText(self.day_data['measured_weight'])
		self.radioButtonNone.setAutoExclusive(False)
		self.radioButtonMedium.setAutoExclusive(False)
		self.radioButtonIntense.setAutoExclusive(False)
		self.radioButtonNone.setChecked(False)
		self.radioButtonMedium.setChecked(False)
		self.radioButtonIntense.setChecked(False)
		if self.day_data['activity_level'] == 'none':
			self.radioButtonNone.setChecked(True)
		elif self.day_data['activity_level'] == 'medium':
			self.radioButtonMedium.setChecked(True)
		elif self.day_data['activity_level'] == 'intense':
			self.radioButtonIntense.setChecked(True)
		self.radioButtonNone.setAutoExclusive(True)
		self.radioButtonMedium.setAutoExclusive(True)
		self.radioButtonIntense.setAutoExclusive(True)
		self.tableDiet.setRowCount(0)
		self.tableDiet.setRowCount(len(self.day_data['diet']))
		for r in range(len(self.day_data['diet'])):
			food,calories = self.day_data['diet'][r]
			self.tableDiet.setItem(r, 0, QTableWidgetItem(str(food)))
			self.tableDiet.setItem(r, 1, QTableWidgetItem(str(calories)))
		self.labelCaloriesConsumed.setPayload(calories_consumed)
		self.labelCaloriesBurned.setPayload(calories_burned)
		self.labelCalorieDifference.setPayload(calorie_difference)
		self.labelWeightDifference.setPayload(weight_difference)
		self.checkBoxDataComplete.setChecked(self.day_data['data_complete'])

	def save_data(self):
		if not self.parent.user.strip():
			return
		self.day_data['measured_weight'] = self.lineEditMeasuredWeight.text()
		if self.radioButtonNone.isChecked():
			self.day_data['activity_level'] = 'none'
		elif self.radioButtonMedium.isChecked():
			self.day_data['activity_level'] = 'medium'
		elif self.radioButtonIntense.isChecked():
			self.day_data['activity_level'] = 'intense'
		else:
			self.day_data['activity_level'] = ''
		self.day_data['diet'] = []
		for r in range(self.tableDiet.rowCount()):
			try:
				food = self.tableDiet.item(r,0).text()
			except:
				food = ''
			try:
				calories = self.tableDiet.item(r,1).text()
			except:
				calories = ''
			if food or calories:
				self.day_data['diet'].append([food, calories])
		self.day_data['data_complete'] = self.checkBoxDataComplete.isChecked()
		self.parent.user_data[self.parent.user]['dates'][self.parent.viewDate] = self.day_data
		with open(self.parent.user_data_path, 'w') as f:
			json.dump(self.parent.user_data, f, indent=4)
		self.parent.read_data()

	def showDateDialog(self):
		self.dateDialog = DateDialog(self.parent.viewDate)
		self.dateDialog.show()
		if not self.dateDialog.date_selected:
			return
		self.parent.viewDate = self.dateDialog.date
		self.update_display()


class TrendView():

	def __init__(self, parent):
		self.parent = parent
		self.start_date = datetime.today().strftime('%Y%m%d')
		self.end_date = datetime.today().strftime('%Y%m%d')
		self.futureCalories = ''
		self.activityLevel = ''
		self.data_computer = DataComputation(self.parent)
		self.drawMainWindow()
	
	def update_display(self):
		self.labelStartDate.setDate(self.start_date)
		self.labelEndDate.setDate(self.end_date)
		self.lineEditFutureCalories.setText(self.futureCalories)
		if self.activityLevel == 'none':
			self.radioButtonNone.setChecked(True)
		elif self.activityLevel == 'medium':
			self.radioButtonMedium.setChecked(True)
		elif self.activityLevel == 'intense':
			self.radioButtonIntense.setChecked(True)
		self.clearPlot()
	
	def drawMainWindow(self):
		self.mainWidget = QWidget()
		self.mainLayout = QVBoxLayout(self.mainWidget)
		self.createTopControls()
		self.createPlotView()
		self.update_display()

	def createPlotView(self):
		'''
		Method to create the plot view and add it to the main window...
		'''
		plotLayout = QVBoxLayout()
		plotLayout.addStretch()
		self.plotCanvas = PlotCanvas(self)
#		self.plotCanvas.setFixedHeight(400)
#		self.plotCanvas.setFixedWidth(500)
		plotLayout.addWidget(self.plotCanvas)
		self.mainLayout.addLayout(plotLayout)
	
	def createTopControls(self):
		'''
		Method to add the top controls to the main window...
		'''
		spacing = 10
		layoutTopControls = QVBoxLayout()
		#Spacing
		layoutTopControls.addSpacing(spacing)
		#Start date controls - row 1
		layoutStartDate1 = QHBoxLayout()
		self.buttonStartDatePrev = QPushButton()
		self.buttonStartDatePrev.setIcon(QtGui.QIcon(os.path.join(self.parent.picturedir, "back_arrow.png")))
		self.buttonStartDatePrev.clicked.connect(self.prevStartDate)
		layoutStartDate1.addWidget(self.buttonStartDatePrev)
		self.labelStartDate = DateLabel("     Start Date: ")
		layoutStartDate1.addWidget(self.labelStartDate)
		self.buttonStartDateNext = QPushButton()
		self.buttonStartDateNext.setIcon(QtGui.QIcon(os.path.join(self.parent.picturedir, "forward_arrow.png")))
		self.buttonStartDateNext.clicked.connect(self.nextStartDate)
		layoutStartDate1.addWidget(self.buttonStartDateNext)
		layoutTopControls.addLayout(layoutStartDate1)
		#Start date controls - row 2
		layoutStartDate2 = QHBoxLayout()
		self.buttonSelectStartDate = QPushButton("Select Start Date")
		self.buttonSelectStartDate.clicked.connect(self.selectStartDate)
		layoutStartDate2.addWidget(self.buttonSelectStartDate)
		self.buttonStartDateToday = QPushButton("Today")
		self.buttonStartDateToday.clicked.connect(self.startDateToday)
		layoutStartDate2.addWidget(self.buttonStartDateToday)
		layoutTopControls.addLayout(layoutStartDate2)
		#Start date controls - row 3
		layoutStartDate3 = QHBoxLayout()
		self.buttonStartDateOfFirstMeasurement = QPushButton("Date of First Measurement")
		self.buttonStartDateOfFirstMeasurement.clicked.connect(self.startDateOfFirstMeasurement)
		layoutStartDate3.addWidget(self.buttonStartDateOfFirstMeasurement)
		self.buttonStartDateOfLastMeasurement = QPushButton("Date of Last Measurement")
		self.buttonStartDateOfLastMeasurement.clicked.connect(self.startDateOfLastMeasurement)
		layoutStartDate3.addWidget(self.buttonStartDateOfLastMeasurement)
		self.buttonStartOffset = QPushButton("Offset")
		self.buttonStartOffset.clicked.connect(self.startOffset)
		layoutStartDate3.addWidget(self.buttonStartOffset)
		layoutTopControls.addLayout(layoutStartDate3)
		#Horizontal line
		layoutTopControls.addWidget(HorizontalLine())
		#End date controls - row 1
		layoutEndDate1 = QHBoxLayout()
		self.buttonEndDatePrev = QPushButton()
		self.buttonEndDatePrev.setIcon(QtGui.QIcon(os.path.join(self.parent.picturedir, "back_arrow.png")))
		self.buttonEndDatePrev.clicked.connect(self.prevEndDate)
		layoutEndDate1.addWidget(self.buttonEndDatePrev)
		self.labelEndDate = DateLabel("      End Date: ")
		layoutEndDate1.addWidget(self.labelEndDate)
		self.buttonEndDateNext = QPushButton()
		self.buttonEndDateNext.setIcon(QtGui.QIcon(os.path.join(self.parent.picturedir, "forward_arrow.png")))
		self.buttonEndDateNext.clicked.connect(self.nextEndDate)
		layoutEndDate1.addWidget(self.buttonEndDateNext)
		layoutTopControls.addLayout(layoutEndDate1)
		#End date controls - row 2
		layoutEndDate2 = QHBoxLayout()
		self.buttonSelectEndDate = QPushButton("Select End Date")
		self.buttonSelectEndDate.clicked.connect(self.selectEndDate)
		layoutEndDate2.addWidget(self.buttonSelectEndDate)
		self.buttonEndDateToday = QPushButton("Today")
		self.buttonEndDateToday.clicked.connect(self.endDateToday)
		layoutEndDate2.addWidget(self.buttonEndDateToday)
		layoutTopControls.addLayout(layoutEndDate2)
		#End date controls - row 3
		layoutEndDate3 = QHBoxLayout()
		self.buttonEndDateOfFirstMeasurement = QPushButton("Date of First Measurement")
		self.buttonEndDateOfFirstMeasurement.clicked.connect(self.endDateOfFirstMeasurement)
		layoutEndDate3.addWidget(self.buttonEndDateOfFirstMeasurement)
		self.buttonEndDateOfLastMeasurement = QPushButton("Date of Last Measurement")
		self.buttonEndDateOfLastMeasurement.clicked.connect(self.endDateOfLastMeasurement)
		layoutEndDate3.addWidget(self.buttonEndDateOfLastMeasurement)
		self.buttonEndOffset = QPushButton("Offset")
		self.buttonEndOffset.clicked.connect(self.endOffset)
		layoutEndDate3.addWidget(self.buttonEndOffset)
		layoutTopControls.addLayout(layoutEndDate3)
		#Horizontal line
		layoutTopControls.addWidget(HorizontalLine())
		#Predicted future calories controls
		layoutFutureCalories = QHBoxLayout()
		labelFutureCalories = QLabel("Predicted future daily calories: ")
		layoutFutureCalories.addWidget(labelFutureCalories)
		self.lineEditFutureCalories = QLineEdit()
		self.lineEditFutureCalories.textChanged.connect(self.setInstanceVariables)
		layoutFutureCalories.addWidget(self.lineEditFutureCalories)
		self.buttonAverageFromStartDate = QPushButton("Average from Start Date")
		self.buttonAverageFromStartDate.clicked.connect(self.average_calories_from_start_date)
		layoutFutureCalories.addWidget(self.buttonAverageFromStartDate)
		layoutTopControls.addLayout(layoutFutureCalories)
		#Spacing
		layoutTopControls.addSpacing(spacing)
		#Predicted future activity level controls
		layoutFutureActivityLevel = QHBoxLayout()
		labelFutureActivityLevel = QLabel("Predicted future activity level: ")
		layoutFutureActivityLevel.addWidget(labelFutureActivityLevel)
		layoutActivityLevels = QVBoxLayout()
		self.radioButtonNone = QRadioButton("Little to no exersize")
		self.radioButtonNone.setToolTip("Sedentary for most or all of the day.")
		self.radioButtonNone.toggled.connect(self.setInstanceVariables)
		layoutActivityLevels.addWidget(self.radioButtonNone)
		self.radioButtonMedium = QRadioButton("Some exersize")
		self.radioButtonMedium.setToolTip("Active for part of the day (like going to the gym for 30 minutes to a few hours).")
		self.radioButtonMedium.toggled.connect(self.setInstanceVariables)
		layoutActivityLevels.addWidget(self.radioButtonMedium)
		self.radioButtonIntense = QRadioButton("Intense exersize or sustained activity")
		self.radioButtonIntense.setToolTip("Active for a large portion of the day (like a physical job), or exersized intensely throughout the day (like athletic training).")
		self.radioButtonIntense.toggled.connect(self.setInstanceVariables)
		layoutActivityLevels.addWidget(self.radioButtonIntense)
		layoutFutureActivityLevel.addLayout(layoutActivityLevels)
		layoutTopControls.addLayout(layoutFutureActivityLevel)
		#Horizontal line
		layoutTopControls.addWidget(HorizontalLine())
		#Plot buttons
		layoutPlotControls = QHBoxLayout()
		self.buttonPlotWeight = QPushButton("Plot Weight")
		self.buttonPlotWeight.clicked.connect(self.plotWeight)
		layoutPlotControls.addWidget(self.buttonPlotWeight)
		self.buttonClearPlot = QPushButton("Clear Plot")
		self.buttonClearPlot.clicked.connect(self.clearPlot)
		layoutPlotControls.addWidget(self.buttonClearPlot)
		layoutTopControls.addLayout(layoutPlotControls)
		self.mainLayout.addLayout(layoutTopControls)

	def average_calories_from_start_date(self):
		today = datetime.today().strftime('%Y%m%d')
		if Utils.date_diff(self.start_date, today) > 0:
			md = MessageDialog()
			md.show("Start date cannot come after today.")
			return
		average_calories = self.data_computer.average_calories_between_dates(self.start_date, today)
		if average_calories is None:
			return
		self.lineEditFutureCalories.setText(f"{average_calories:.0f}")
		self.update_display()

	def setInstanceVariables(self):
		self.futureCalories = self.lineEditFutureCalories.text()
		self.activityLevel = ''
		if self.radioButtonNone.isChecked():
			self.activityLevel = 'none'
		elif self.radioButtonMedium.isChecked():
			self.activityLevel = 'medium'
		elif self.radioButtonIntense.isChecked():
			self.activityLevel = 'intense'
		self.update_display()
	
	def startDateOfFirstMeasurement(self):
		try:
			self.start_date = self.data_computer.get_dates_with_measured_weights()[0]
			self.update_display()
		except:
			md = MessageDialog()
			md.show(f"Cannot find any measurements for user: {self.parent.user}")

	def startDateOfLastMeasurement(self):
		try:
			self.start_date = self.data_computer.get_dates_with_measured_weights()[-1]
			self.update_display()
		except:
			md = MessageDialog()
			md.show(f"Cannot find any measurements for user: {self.parent.user}")

	def startOffset(self):
		dod = DateOffsetDialog()
		dod.show()
		if dod.canceled:
			return
		try:
			years = float(dod.years)
		except:
			years = 0
		try:
			months = float(dod.months)
		except:
			months = 0
		try:
			days = float(dod.days)
		except:
			days = 0
		self.start_date = Utils.increment_date(self.start_date, years=years, months=months, days=days)
		self.update_display()

	def endDateOfFirstMeasurement(self):
		try:
			self.end_date = self.data_computer.get_dates_with_measured_weights()[0]
			self.update_display()
		except:
			md = MessageDialog()
			md.show(f"Cannot find any measurements for user: {self.parent.user}")

	def endDateOfLastMeasurement(self):
		try:
			self.end_date = self.data_computer.get_dates_with_measured_weights()[-1]
			self.update_display()
		except:
			md = MessageDialog()
			md.show(f"Cannot find any measurements for user: {self.parent.user}")

	def endOffset(self):
		dod = DateOffsetDialog()
		dod.show()
		if dod.canceled:
			return
		try:
			years = float(dod.years)
		except:
			years = 0
		try:
			months = float(dod.months)
		except:
			months = 0
		try:
			days = float(dod.days)
		except:
			days = 0
		self.end_date = Utils.increment_date(self.end_date, years=years, months=months, days=days)
		self.update_display()
	
	def prevStartDate(self):
		current_date = datetime.strptime(self.start_date, '%Y%m%d')
		new_date = current_date - timedelta(days=1)
		self.start_date = new_date.strftime('%Y%m%d')
		self.update_display()

	def nextStartDate(self):
		current_date = datetime.strptime(self.start_date, '%Y%m%d')
		new_date = current_date + timedelta(days=1)
		self.start_date = new_date.strftime('%Y%m%d')
		self.update_display()

	def prevEndDate(self):
		current_date = datetime.strptime(self.end_date, '%Y%m%d')
		new_date = current_date - timedelta(days=1)
		self.end_date = new_date.strftime('%Y%m%d')
		self.update_display()

	def nextEndDate(self):
		current_date = datetime.strptime(self.end_date, '%Y%m%d')
		new_date = current_date + timedelta(days=1)
		self.end_date = new_date.strftime('%Y%m%d')
		self.update_display()

	def selectStartDate(self):
		dd = DateDialog(self.start_date)
		dd.show()
		if dd.date_selected:
			self.start_date = dd.date
			self.update_display()

	def startDateToday(self):
		self.start_date = datetime.today().strftime('%Y%m%d')
		self.update_display()

	def selectEndDate(self):
		dd = DateDialog(self.end_date)
		dd.show()
		if dd.date_selected:
			self.end_date = dd.date
			self.update_display()

	def endDateToday(self):
		self.end_date = datetime.today().strftime('%Y%m%d')
		self.update_display()

	def generate_data(self):
		#Populate list of dates to be based on past data, as well as a list to be based on future predictions.
		past_dates = []
		future_dates = []
		tempdate = self.start_date
		today = datetime.today().strftime('%Y%m%d')
		while Utils.date_diff(self.end_date, tempdate) >= 0:
			if Utils.date_diff(tempdate, today) <= 0:
				past_dates.append(tempdate)
			else:
				future_dates.append(tempdate)
			tempdate = Utils.increment_date(tempdate, days=1)
		#Generate past data.
		dates_past = []
		Y_past = []
		for date in past_dates:
			dates_past.append(date)
			computed_data = self.data_computer.compute_data(date)
			Y_past.append(float(computed_data['predicted_weight']))
		#Generate future data.
		dates_future = []
		Y_future = []
		if len(future_dates):
			dates_future, Y_future = self.data_computer.compute_forecast(future_dates[-1], self.futureCalories, self.activityLevel)
		#Combine the past and future data sets.
		dates = dates_past + dates_future
		X = []
		x = 0
		for date in dates:
			X.append(x)
			x += 1
		Y = Y_past + Y_future
		#Return the result.
		return (dates, X, Y)

	def input_data_check(self):
		#Verify that a user is selected and get the user's data.
		try:
			user = self.parent.user_data[self.parent.user]
		except:
			md = MessageDialog()
			md.show("Please select a user.")
			return False
		#Verify that the user has some weight measurements in their user data.
		dates_with_measured_weights = list(val for val in user['dates'].keys() if user['dates'][val]['measured_weight'])
		dates_with_measured_weights.sort()
		if not dates_with_measured_weights:
			md = MessageDialog()
			md.show(f"User {self.parent.user} does not have any measured weight information.")
			return False
		#Verify that the start date doesn't come after the current date.
		today = datetime.today().strftime('%Y%m%d')
		if Utils.date_diff(self.start_date, today) > 0:
			md = MessageDialog()
			md.show(f"The start date cannot be in the future.\nSetting the start date to {today}.")
			self.start_date = today
			self.update_display()
		#Verify that the start date doesn't come before the first date with a measured weight.
		dtstart = datetime.strptime(self.start_date, '%Y%m%d')
		first_date_with_measured_weight = datetime.strptime(dates_with_measured_weights[0], '%Y%m%d')
		diff = (dtstart - first_date_with_measured_weight).days
		if diff < 0:
			md = MessageDialog()
			md.show(f"The start date cannot be before the date of first weight measurement.\nSetting start date to {dates_with_measured_weights[0]}.")
			self.start_date = dates_with_measured_weights[0]
			self.update_display()
		#Check that end date comes after the start date.
		dtend = datetime.strptime(self.end_date, '%Y%m%d')
		diff = (dtend - dtstart).days
		if diff <= 0:
			md = MessageDialog()
			md.show("End date must come after the start date.")
			return False
		#If the user intends to view future data...
		if Utils.date_diff(self.end_date, datetime.today().strftime('%Y%m%d')) > 0:
			#Check that a number was entered for predicted future calories.
			try:
				float(self.lineEditFutureCalories.text())
			except:
				md = MessageDialog()
				md.show("Please enter a number for the predicted future calories.")
				return False
			#Check that exactly one selection was made for future activity level (it shouldn't be possible to select multiples, but we'll check anyway).
			num_selections = 0
			if self.radioButtonNone.isChecked():
				num_selections += 1
			if self.radioButtonMedium.isChecked():
				num_selections += 1
			if self.radioButtonIntense.isChecked():
				num_selections += 1
			if num_selections != 1:
				md = MessageDialog()
				md.show("Please select an option for activity level.")
				return False
		return True

	def plotWeight(self):
		if not self.input_data_check():
			return
		self.futureCalories = self.lineEditFutureCalories.text()
		dates, X, Y = self.generate_data()
		self.plotCanvas.plot(X, Y)
	
	def clearPlot(self):
		self.plotCanvas.clear()

