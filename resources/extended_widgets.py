#!/usr/bin/env python

import matplotlib
matplotlib.use('Qt5Agg')
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg
from matplotlib.figure import Figure

from PyQt5.QtWidgets import QLabel, QFrame, QSizePolicy


class HorizontalLine(QFrame):

  def __init__(self):
    super().__init__()
    self.setMinimumWidth(1)
    self.setFixedHeight(20)
    self.setFrameShape(QFrame.HLine)
    self.setFrameShadow(QFrame.Sunken)
    self.setSizePolicy(QSizePolicy.Preferred, QSizePolicy.Minimum)


class VerticalLine(QFrame):

  def __init__(self):
    super().__init__()
    self.setFixedWidth(20)
    self.setMinimumHeight(1)
    self.setFrameShape(QFrame.VLine)
    self.setFrameShadow(QFrame.Sunken)
    self.setSizePolicy(QSizePolicy.Minimum, QSizePolicy.Preferred)


class PrefixedLabel(QLabel):

	def __init__(self, prefix='', payload=''):
		super().__init__()
		self.prefix = prefix
		self.payload = payload
		self.updateText()
	
	def updateText(self):
		self.setText(self.prefix + self.payload)
	
	def setPrefix(self, prefix):
		self.prefix = prefix
		self.updateText()
	
	def setPayload(self, payload):
		self.payload = payload
		self.updateText()


class DateLabel(PrefixedLabel):

	def __init__(self, prefix='', date=''):
		super().__init__(prefix=prefix, payload='')
		if date != '':
			self.setDate(date)
	
	def setDate(self, date):
		formatted_date = ''
		if date != '':
			try:
				formatted_date = date[4:6] + '/' + date[6:8] + '/' + date[0:4]
			except: pass
		self.setPayload(formatted_date)


class PlotCanvas(FigureCanvasQTAgg):

	def __init__(self, parent, width=5, height=5, dpi=100):
		self.parent = parent
		fig = Figure(figsize=(width, height), dpi=dpi)
		self.ax = fig.add_subplot(1,1,1)
		super().__init__(fig)

	def clear(self):
		self.ax.cla()
		self.draw()
	
	def plot(self, X, Y):
		self.clear()
		self.ax.plot(X, Y)
		self.draw()
